;;; librelounge.org website
;;; Copyright © 2016 Christopher Lemmer Webber <cwebber@dustycloud.org>
;;;
;;; Some code borrowed from David Thompson's website
;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; GPLv3 is directly compatible with CC BY-SA 4.0, but for avoidance
;;; of confusion in mixing with website content the following code is
;;; also available under CC BY-SA 4.0 International, as published by
;;; Creative Commons.

(use-modules (ice-9 match)
             (srfi srfi-1)
             (srfi srfi-11)
             (haunt asset)
             (haunt html)
             (haunt site)
             (haunt page)
             (haunt post)
             (haunt utils)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder rss)
             (haunt builder assets)
             (haunt reader)
             (haunt reader skribe)
             (haunt reader commonmark)
             (web uri))


;;; Utilities
;;; ---------

(define %site-prefix (make-parameter ""))

(define (prefix-url url)
  (string-append (%site-prefix) url))


;;; Templates
;;; ---------

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(prefix-url (string-append "/static/css/" name ".css"))))))

(define (base-image image alt)
  `(img (@ (src ,(prefix-url (string-append "/static/images/" image)))
           (alt ,alt))))

(define (header-menu)
  `(("subscribe" ,(prefix-url "/#subscribe"))
    ("archive" ,(prefix-url "/archive/"))
    ("about" ,(prefix-url "/about/"))
    ("contact" ,(prefix-url "/contact/"))))

(define* (base-tmpl site body
                    #:key title #;big-logo)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (title ,(if title
                 (string-append title " -- Libre Lounge")
                 (site-title site)))
     ;; css
     ,(stylesheet "main")
     #;(link (@ (rel "stylesheet")
              (href ,(stylesheet "code.css"))))
     ;; atom feed
     #;(link (@ (rel "alternate")
              (title "Libre Lounge")
              (type "application/atom+xml")
              (href ,(prefix-url "/atom-feed.atom"))))
     ;; rss feed
     (link (@ (rel "alternate")
              (title "Libre Lounge")
              (type "application/rss+xml")
              (href ,(prefix-url "/rss-feed.rss")))))
    (body
     (div (@ (class "main-wrapper"))
          (header (@ (id "site-header"))
                  ;; Site logo
                  (div (@ (class "header-logo-wrapper"))
                       (a (@ (href ,(prefix-url "/"))
                             (class "header-logo"))
                          ,(base-image "emacsy-logo-smaller-nocaps.png"
                            #;(if big-logo
                                "librelounge-logo-500px.png"
                                "librelounge-logo-300px.png")
                            "Libre Lounge: a casual podcast about user freedom")))
                  ;; Header menu
                  (div (@ #;,(if big-logo
                               '(class "navbar-menu big-navbar")
                               '(class "navbar-menu small-navbar"))
                        (class "navbar-menu navbar"))
                       "-=* "
                       ,@(map
                          (lambda (item)
                            (match item
                              ((name url)
                               `(div
                                 (a (@ (href ,url))
                                    ,name)))))
                          (header-menu))
                       " *=-"))
          (div (@ (class "site-main-content"))
               ,body))
     ;; TODO: Link to source.
     (div (@ (class "footer"))
          (a (@ (href "https://gitlab.com/librelounge/librelounge.org"))
             "Site contents")
          " released under "
          (a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
             "Creative Commons Attribution-Sharealike 4.0 International")
          ".  Powered by "
          (a (@ (href "http://haunt.dthompson.us/"))
             "Haunt")
          "."))))

(define* (post-template post #:key post-link)
  (define enclosures
    (reverse (post-ref-all post 'enclosure)))
  `(div (@ (class "content-box blogpost"))
        (h1 (@ (class "title"))
            ,(if post-link
                 `(a (@ (href ,post-link))
                     ,(post-ref post 'title))
                 (post-ref post 'title)))
        (div (@ (class "post-about"))
             (span (@ (class "by-line"))
                   ,(post-ref post 'author))
             " -- " ,(date->string* (post-date post)))
        (audio (@ (controls "")
                  (preload "none")
                  (class "post-audio")
                  (style "margin-top: 1em;"))
               ,@(map audio-source enclosures))
        (div (@ (class "post-download"))
             ,@(map download-button enclosures))

        (div (@ (class "post-body"))
             ,(post-sxml post))))

(define (post-uri site post)
  (prefix-url
   (string-append "/episodes/" (site-post-slug site post) ".html")))

(define (collection-template site title posts prefix)
  ;; In our case, we ignore the prefix argument because
  ;; the filename generated and the pathname might not be the same.
  ;; So we use (prefix-url) instead.
  `((div (@ (class "episodes-header"))
         (h3 "recent episodes"))
    (div (@ (class "post-list"))
         ,@(map
            (lambda (post)
              (post-template post #:post-link (post-uri site post)))
            posts))))

(define librelounge-haunt-theme
  (theme #:name "Libre Lounge"
         #:layout
         (lambda (site title body)
           (base-tmpl
            site body
            #:title title))
         #:post-template post-template
         #:collection-template collection-template))

;; Borrowed from davexunit's blog
(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define* (element-join items #:key [delim " "])
  (let lp ((items items)
           (count 0))
    (match items
      ('() '())
      ((item rest ...)
       (cond
        ((zero? count)
         (cons item (lp rest (1+ count))))
        (else
         (cons delim (cons item (lp rest (1+ count))))))))))

(define (download-button enclosure)
  (define title-text
    (cond
     [(enclosure-title enclosure) =>
      (lambda (title)
        `(": " ,title))]
     [else '("")]))
  `(a (@ (href ,(enclosure-url enclosure)))
      "[Download" ,@title-text "]"))

(define (audio-source enclosure)
  `(source (@ (src ,(enclosure-url enclosure))
              (type ,(enclosure-mime-type enclosure)))))

(define (post-preview post site)
  `(li (a (@ (href ,(post-uri site post)))
          ,(post-ref post 'title))
       (div (@ (class "news-feed-content"))
            (div (@ (class "news-feed-item-date"))
                 ,(date->string* (post-date post)))
            ,(first-paragraph post)
            (div (@ (class "consume-more-buttons"))
                 ,@(element-join
                    (cons
                     `(a (@ (href ,(post-uri site post)))
                         "[Read more ==>]")
                     (map download-button
                          (reverse
                           (post-ref-all post 'enclosure)))))))))


;;; Pages
(define (index-content site posts)
  `(div
    ;; Main intro
    (div (@ (class "content-box bigger-text")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 (@ (class "title"))
             "What's this?")
         (p "Libre Lounge is a podcast where we casually discuss various "
            "topics involving user freedom, crossing free software, free culture, "
            "network and hosting freedom, and libre hardware designs. "
            "We discuss everything from policy and licensing to deep dives on "
            "technical topics... whatever seems interesting that week. "
            "Sometimes we even have guests!")
         (p "Libre Lounge's usual hosts are "
            (a (@ (href "https://dustycloud.org/"))
               "Christopher Lemmer Webber")
            " and "
            (a (@ (href "https://blog.emacsen.net/"))
               "Serge Wroclawski") ".")
         #;(div (@ (class "code"))
              ,(highlights->sxml
                (highlight lex-scheme code-snippet)))
         (h1 (@ (class "title")
                (id "subscribe"))
             "Sounds fun!  How do I listen?")
         (p "Subscribe with your favorite podcatcher via:")
         (ul (li (a (@ (href ,(prefix-url "/rss-feed.rss")))
                    "RSS"))
             (li (a (@ (href ,(prefix-url "/atom-feed.atom")))
                    "Atom")))
         (p "Don't have a favorite podcatcher?  We like "
            (a (@ (href "https://antennapod.org/"))
               "Antennapod") "."))

    ;; News updates and etc
    (div (@ (class "content-box homepage-news-box")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 (@ (class "title"))
             "Episodes and news")
         (ul (@ (class "homepage-news-items"))
             ,@(map (lambda (post)
                      (post-preview post site))
                    (take-up-to 10 (posts/reverse-chronological posts))))
         (p (@ (style "text-align: center"))
            (a (@ (href "/archive/"))
               "[--archive--]")))))

(define (archive-content site posts)
  `(div (@ (class "content-box bigger-text")
	   (style "margin-top: 20px; margin-bottom: 20px;"))
        (h1 (@ (class "title"))
	    "The Archive")
        (ul (@ (class "homepage-news-items"))
            ,@(map (lambda (post)
                     (post-preview post site))
                   (posts/reverse-chronological posts)))))

(define (about-content)
  '(div
    (div (@ (class "content-box bigger-text")
	    (style "margin-top: 20px; margin-bottom: 20px;"))
	 (h1 (@ (class "title"))
	     "About Libre Lounge")
	 (p "Libre Lounge is a podcast by Chris Webber and Serge Wroclawski "
	    "where we discuss Software Freedom, Free Culture, Digital Privacy "
	    "and other topics. We feature news, thoughtful commentary, "
	    "instruction and interviews in a relaxed, fun format"))))

(define (contact-content)
  '(div
    (div (@ (class "content-box bigger-text")
	    (style "margin-top: 20px; margin-bottom: 20px;"))
	 (h1 (@ (class "title"))
	     "Contact Us")
	 (p "You can contact the Libre Lounge crew a number of ways!")
         (ul
	  (li "Email us at podcast at librelounge.org")
	  (li "Find us on IRC at "
              (a (@ (href "https://webchat.freenode.net/?channels=librelounge"))
                 "#librelounge on Freenode"))
	  (li "We're " (a (@ (href "https://floss.social/@librelounge"))
                          "on the fediverse"))
	  (li "We're on Twitter at " (a (@ (href "https://twitter.com/librelounge"))
                                        "@LibreLounge"))))))

(define (index-page site posts)
  (make-page
   "index.html"
   (base-tmpl site
              (index-content site posts)
              ;; #:big-logo #t
              )
   sxml->html))

(define (about-page site posts)
  (make-page
   "/about/index.html"
   (base-tmpl site
	      (about-content))
   sxml->html))

(define (contact-page site posts)
  (make-page
   "/contact/index.html"
   (base-tmpl site
	      (contact-content))
   sxml->html))

(define (archive-page site posts)
  (make-page
   "/archive/index.html"
   (base-tmpl site
	      (archive-content site posts))
   sxml->html))
	      


;;; RSS feed transformations for itunes
(define* (adjust-rss-feed author summary owner-email image
                          #:key (file-name "feed.xml")
                          (owner-name author)
                          (explicit? #f)
                          (itunes-categories '()))
  (lambda (feed site posts)
    (define (add-atom-ns feed)
      (sxml-acons feed 'xmlns:itunes "http://www.itunes.com/dtds/podcast-1.0.dtd"))
    (define (add-itunes-ns feed)
      (sxml-acons feed 'xmlns:atom "http://www.w3.org/2005/Atom"))
    (define (channel+ feed item)
      (match feed
        [('rss ('@ feed-attribs ...)
               channel)
         `(rss (@ ,@feed-attribs)
               ,(sxml-cons channel item))]))
    (define (add-language-tag feed)
      (channel+ feed '(language "en-us")))
    (define (add-copyright feed)
      (channel+ feed  '(copyright "Licensed under CC BY-SA 4.0 International")))
    (define (add-atom-linkhref feed)
      (channel+ feed
                `(atom:link
                  (@ (href ,(uri->string
                             (add-path-to-uri (site-base-uri site)
                                              file-name)))
                     (rel "self")))))
    (define (add-itunes-author feed)
      (channel+ feed `(itunes:author ,author)))
    (define (add-itunes-owner feed)
      (channel+ feed `(itunes:owner
                       (itunes:name ,owner-name)
                       (itunes:email ,owner-email))))
    (define (add-itunes-summary feed)
      (channel+ feed `(itunes:summary ,summary)))
    (define (add-itunes-explicit feed)
      (channel+ feed
                (if explicit?
                    '(itunes:explicit "yes")
                    '(itunes:explicit "no"))))
    (define (add-itunes-image feed)
      (channel+ feed
                `(itunes:image (@ (href ,image)))))
    (define (add-itunes-categories feed)
      (fold (lambda (category prev)
              (match category
                [(? string? category)
                 (channel+ prev
                           `(itunes:category (@ (text ,category))))]
                [((? string? category)
                  (subcategories ...))
                 (let ([subcategories-sxml
                        (map (lambda (sc)
                               `(itunes:category (@ (text ,sc))))
                             subcategories)])
                   (channel+ prev
                             `(itunes:category (@ (text ,category))
                                               ,@subcategories-sxml)))]))
            feed itunes-categories))
    (define (add-author feed)
      (channel+ feed
                `(author
                  ,(string-append (if owner-email
                                      (string-append owner-email " ")
                                      "")
                                  (if author
                                      (string-append "(" author ")")
                                      "")))))
    (define (add-image feed)
      (channel+ feed
                `(image (url ,image)
                        (title "Libre Lounge")
                        (link ,(uri->string (site-base-uri site))))))
    (define munge-it
      (compose add-atom-ns add-itunes-ns add-language-tag
               add-atom-linkhref add-copyright
               add-itunes-author add-itunes-owner add-itunes-summary
               add-itunes-explicit add-itunes-image add-itunes-categories
               add-author add-image))
    (munge-it feed)))

(define* (adjust-rss-item image
                          #:key [explicit? #t])
  (lambda (item site post)
    (define (add-itunes-image item)
      (sxml-cons item
                 `(itunes:image (@ (href ,image)))))
    (define (add-itunes-explicit item)
      (sxml-cons item
                 (if explicit?
                    '(itunes:explicit "yes")
                    '(itunes:explicit "no"))))
    (define munge-it
      (compose add-itunes-image add-itunes-explicit))
    (munge-it item)))

(define (adjust-rss-enclosure-duration sxml site post enclosure)
  ;; Add the itunes duration property
  (match (assoc 'duration (enclosure-extra enclosure))
    [(_ . val)
     (sxml-acons sxml 'itunes:duration val)]
    [#f sxml]))



;;; Site

(define ll-subtitle
  "A casual podcast about user freedom")
(define ll-logo-url
  "https://librelounge.org/static/images/ll-logo-square.png")

(site #:title "Libre Lounge"
      #:base-uri (string->uri "https://librelounge.org")
      #:default-metadata
      '((author . "Libre Lounge")
        (email . "podcast@librelounge.org"))
      #:readers (list skribe-reader commonmark-reader)
      #:builders (list (blog #:prefix "/episodes"
                             #:theme librelounge-haunt-theme)
                       index-page
                       (atom-feed #:blog-prefix "/episodes"
                                  #:file-name "atom-feed.atom")
                       ;; We used to have a feed at this uri, so render a duplicate
                       ;; atom feed here for backwards compatibility
                       (atom-feed #:blog-prefix "/episodes"
                                  #:file-name "atom-feed.xml"
                                  #:subtitle ll-subtitle)
                       (rss-feed #:blog-prefix "/episodes"
                                 #:file-name "rss-feed.rss"
                                 #:subtitle ll-subtitle
                                 #:adjust-feed
                                 (list
                                  (adjust-rss-feed
                                   "Libre Lounge"
                                   ll-subtitle "podcast@librelounge.org"
                                   ll-logo-url
                                   #:explicit? #t
                                   #:itunes-categories '(("Technology"
                                                          ("Computers"
                                                           "Developers"
                                                           "Operating Systems")))))
                                 #:adjust-item
                                 (list (adjust-rss-item ll-logo-url))
                                 #:adjust-enclosure
                                 (list adjust-rss-enclosure-duration))
                       (static-directory "static" "static")
		       about-page
		       contact-page
		       archive-page
                       (atom-feeds-by-tag)))

title: Episode 23: Guix with Ludovic Courtès
date: 2019-06-21 16:00
tags: episode, guix, distros, package managers, reproducibility
summary: Guix with Ludovic Courtès
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/023-guix-with-ludo/librelounge-ep-023.mp3" length:"45092336" duration:"00:52:32"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/023-guix-with-ludo/librelounge-ep-023.ogg" length:"39871357" duration:"00:52:32"
---
On this episode of Libre Lounge we get on Ludovic Courtès to talk about the [Guix](https://www.gnu.org/software/guix/) package manager and distribution, functional package management, reproducibility, and bootstrapping!

Links:

 - [GNU Guix](https://www.gnu.org/software/guix/)
 - [GNU Mes](https://www.gnu.org/software/mes/)
 - [Bootstrappable Builds](http://bootstrappable.org/)
 - [Reflections on Trusting Trust (the "Thompson Attack")](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf)
 - [Stage0](https://bootstrapping.miraheze.org/wiki/Stage0)
 - [Guix 1.0 released (May 2, 2019)](https://www.gnu.org/software/guix/blog/2019/gnu-guix-1.0.0-released/)

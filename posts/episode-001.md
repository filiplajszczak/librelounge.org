title: Episode 1: Corporate control, org-mode, mobile phones and PDAs
date: 2018-11-20 16:00:00
tags: episode, org-mode
summary: The premiere episode of Libre Lounge
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/001/librelounge-ep-001.mp3" length:"23915418" duration:"00:57:37"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/001/librelounge-ep-001.ogg" length:"27898599" duration:"00:57:37"
---
In their premiere episode, Chris and Serge jump into a variety of topics: Corporate control of Free Software, Time management systems, Free Software mobile devices and PDAs that ran GNU/Linux.

Come with them in thier first journey into podcasting (and be forgiving)!

Links to some of the things discussed in the show

- [Linux Sucks Forever](https://www.youtube.com/watch?v=TVHcdgrqbHE) - The latest in the "Linux Sucks" videos talking about corporate control of Linux and Free Software in general
- [The Halloween Documents](http://www.catb.org/~esr/halloween/) - The documents describing Microsoft's strategy of "Embrace, Extend, Extinguish"
- [Quality Standards, Service Orientation, and Power in Airbnb and Couchsurfing](http://notconfusing.com/airsurfing.pdf) - Benjamin Mako Hill discussing Couchsurfing
- [On Usage of The Phrase "Open Source"](https://perens.com/2017/09/26/on-usage-of-the-phrase-open-source/) - Bruce Perens describing the origins of Open Source
- [How I coined the term 'open source'](https://opensource.com/article/18/2/coining-term-open-source-software) - Christine Peterson discusses how she invented the term 'Open Source'
- [Hackers: Heroes of the Computer Revolution](https://en.wikipedia.org/wiki/Hackers:_Heroes_of_the_Computer_Revolution) - The book from the 1980s describing the origin of the Hacker movement
- [F-Droid](https://f-droid.org/) - A software repository of Free and Open Source Software for the Android platform
- [Replicant](https://replicant.us/) - A 100% Free Software operating system for mobile phones
- [LineageOS](https://lineageos.org/) - A Free and Open Source operating system for mobile devices
- [LibreM 5](https://puri.sm/products/librem-5/) - A new 100% Free Software, Privacy Oriented Mobile Phone coming soon
- [OpenMoko](http://wiki.openmoko.org/wiki/Main_Page) - A project to create a Free mobile smartphone in/around 2007/2008 that never fully took off
- [Org Mode](https://orgmode.org/) - A system for keeping track of everything in your life in plain text through Emacs
- [Orgzly](http://www.orgzly.com/) - An Org mode compatible editor for Android
- [The Hipster PDA](http://www.43folders.com/2004/09/03/introducing-the-hipster-pda) - The Hipster PDA
- [Time Management for System Administrators](http://shop.oreilly.com/product/9780596007836.do) - The book where Serge learned the Cycle system for time managament
- [Rudel](https://www.emacswiki.org/emacs/Rudel) - Distributed real-time editing editing in Emacs; apparently supports the [Gobby](https://gobby.github.io/) protocol and others (we haven't tried this ourselves!)
- [The Agenda VR3](https://en.wikipedia.org/wiki/Agenda_VR3) - The first Linux-based Personal Digital Assistant
- [Sharp Zaurus](https://en.wikipedia.org/wiki/Sharp_Zaurus) - A more capable Linux-based PDA
- [Emacs appointment notifications via XMPP](https://dustycloud.org/blog/emacs-appointment-notifications-via-xmpp/) - A pretty good notification setup in case you can't project org-mode straight into your eyeballs

title: Episode 16: Behind the Scenes 2019
date: 2019-04-12 02:00
tags: episode, meta
summary: Behind the Scenes 2019
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/016-behind-the-scenes-2019/librelounge-ep-016.mp3" length:"27298378" duration:"01:12:45"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/016-behind-the-scenes-2019/librelounge-ep-016.ogg" length:"38115428" duration:"01:12:45"
---
Are you ready for a meta episode? Curious about how Libre Lounge gets made? This show is for you as Serge and Chris talk about their goals for Libre Lounge and the process for making a show in 2019.

Links:

- [Audacity](https://www.audacityteam.org/)
- [Haunt](https://dthompson.us/projects/haunt.html)
- [Git Annex](https://git-annex.branchable.com/)
- [Archive.org](https://archive.org)


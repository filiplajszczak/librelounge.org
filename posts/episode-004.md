title: Episode 4: The Universal Declaration of Human Rights and Free Software
date: 2018-12-14 00:50:00
tags: episode, free software, universal declaration of human rights
summary: The Universal Declaration of Human Rights and Free Software
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/004/librelounge-ep-004.mp3" length:"29618553" duration:"01:28:55"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/004/librelounge-ep-004.ogg" length:"40361334" duration:"01:28:55"
---
In this episode of Libre Lounge, Chris sits down with Serge to talk about the Universal Declaration of Human rights and discuss how they relate to Free Software.

Show links:

- [The Universal Declaration of Human Rights (un.org)](http://www.un.org/en/universal-declaration-human-rights/)
- [An Animated Version of the Universa Declaration of Human Rights (youtube)](https://www.youtube.com/watch?v=hTlrSYbCbHE)
- [Is Migration a Basic Human Right? (freakonomics.com)](http://freakonomics.com/podcast/is-migration-a-basic-human-right-a-new-freakonomics-radio-podcast/)
- [Chris Webber's Patreon (patreon)](https://www.patreon.com/cwebber/overview)

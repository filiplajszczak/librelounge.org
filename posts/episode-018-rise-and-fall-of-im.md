title: Episode 18: The Rise and Fall of Instant Messengers
date: 2019-04-26 00:00:00
tags: episode, instant messengers
summary: The Rise and Fall of Instant Messengers
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/018-instant-messengers/librelounge-ep-018.mp3" length:"23536670" duration:"00:59:48"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/018-instant-messengers/librelounge-ep-018.ogg" length:"34997353" duration:"00:59:48"
---

Chris and Serge wax poetic about instant messengers of the past, their 
influence on Free Software, on communication and ways that they impacted 
each of their lives.

Content Warning: This episode has a brief discussion of death, including 
suicide.

If you struggle with depression and are having a crisis, please use one of the resources listed at the end.

Show Links:

- [Botten Anna (youtube)](https://www.youtube.com/watch?v=RYQUsp-jxDQ) (look for a lower quality version which has English subtitles)
- [Pidgin Instant Messenger (pidgin.im)](https://www.pidgin.im/)
- [Off the Record Messaging (cypherpunks.ca)](https://otr.cypherpunks.ca/)
- [Conversations.im (conversations.im)](https://conversations.im/)
- [OMEMO (conversations.im)](https://conversations.im/omemo/)
- [Signal (signal.org)](https://signal.org/)

Depression/Suicide Links:

- [Crisis Text Line US](https://crisistext.org)
- [Crisis Text Line Canada](https://crisistextline.ca)
- [List of International Suicide Hotlines](https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines)

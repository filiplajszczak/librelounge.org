title: Episode 2: Thanksgiving, NPM and Malware in Free Software
date: 2018-11-30 16:30:00
tags: episode, npm
summary: NPM and malware in Free Software
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/002/librelounge-ep-002.mp3" length:"33791888" duration:"01:21:38"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/002/librelounge-ep-002.ogg" length:"39680871" duration:"01:21:38"
---
In their second episode, Serge and Chris return from Thanksgiving thinking about malware in Free Software, specifically the NPM bitcoin attack found in event-streamer

Show links:

- [Software Freedom Conservancy (conservancy)](https://sfconservancy.org/)
- [Backdoor in event-stream library dependency (hacker news)](https://news.ycombinator.com/item?id=18534392)
- [The event-stream bug report (github)](https://github.com/dominictarr/event-stream/issues/116)
- [Statement about the event-stream vulerability (bitpay)](https://blog.bitpay.com/npm-package-vulnerability-copay/)
- [npm's statement on the event-stream incident](https://blog.npmjs.org/post/180565383195/details-about-the-event-stream-incident)
- [Bug Report on ESLint (github)](https://github.com/eslint/eslint-scope/issues/39)
- [Malware in Linux kernel (lwn)](https://lwn.net/Articles/57135/)
- [Don't Download Software from Sourceforge (howtogeek.com)](https://www.howtogeek.com/218764/warning-don%E2%80%99t-download-software-from-sourceforge-if-you-can-help-it/)
- [Let's Package jQuery: A Javascript Packaging Dystopian Novella (dustycloud.org)](https://dustycloud.org/blog/javascript-packaging-dystopia/)
- [Reflections on Trusting Trust](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf) - aka the "Thompson attack" mentioned in the episode, a way of embedding malicious code in a compiler that embeds it into the next compiled version of the compiler
- [Zooko's Tweet (twitter)](https://twitter.com/zooko/status/1067204027983777793)
- [Linus's Law (wikipedia)](https://en.wikipedia.org/wiki/Linus%27s_Law)
- [Ka-Ping Yee's dissertation (zesty.ca)](http://zesty.ca/pubs/yee-phd.pdf)
 -[Securing EcmaScript, presentation to Node Security (youtube)](https://www.youtube.com/watch?v=9Snbss_tawI)
- [Mandatory Access Control (wikipedia)](https://en.wikipedia.org/wiki/Mandatory_access_control)
- [SE Linux Project (github)](https://github.com/SELinuxProject)
- [AppArmor (ubuntu)](https://wiki.ubuntu.com/AppArmor)
- [Docker For Development (medium)](https://medium.com/travis-on-docker/why-and-how-to-use-docker-for-development-a156c1de3b24)
- [The Qubes Operating System (qubes)](https://www.qubes-os.org/)
- [Android Application Sandboxing](https://source.android.com/security/app-sandbox)
- [Chris's talk at Northeastern on December 5th](http://prl.ccs.neu.edu/seminars.html#webber-goblins-and-spritely) - Chris gave the wrong date in the episode, it's on Wednesday... oops!

Chris mentioned that they changed their org-mode configuration inspired
by the chat from our
[first episode](/episodes/episode-2-thanksgiving-npm-and-malware-in-free-software.html)
to incorporate a priorities-based workflow.
Maybe you want to look at Chris's updated org-mode configuration!
It looks like so:

```
;; (c) 2018 by Christopher Lemmer Webber
;; Under GPLv3 or later as published by the FSF

;; We want the lowest and "default" priority to be D.  That way
;; when we calculate the agenda, any task that isn't specifically
;; marked with a priority or SCHEDULED/DEADLINE won't show up.
(setq org-default-priority ?D)
(setq org-lowest-priority ?D)

;; Custom agenda dispatch commands which allow you to look at
;; priorities while still being able to see when deadlines, appointments
;; are coming up.  Very often you'll just be looking at the A or B tasks,
;; and when you clear off enough of those or have some time you might
;; look also at the C tasks
;;
;; Hit "C-c a" then one of the following key sequences...
;;  - a for the A priority items, plus the agenda below it
;;  - b for A-B priority items, plus the agenda below it
;;  - c for A-C priority items, plus the agenda below it
;;  - A for just the agenda
;;  - t for just the A-C priority TODOs
(setq org-agenda-custom-commands
      '(("a" "Agenda plus A items"
         ((tags-todo
           "+PRIORITY=\"A\""
           ((org-agenda-sorting-strategy '(priority-down))))
          (agenda "")))
        ("b" "Agenda plus A+B items"
         ((tags-todo
           "+PRIORITY=\"A\"|+PRIORITY=\"B\""
           ((org-agenda-sorting-strategy '(priority-down))))
          (agenda "")))
        ("c" "Agenda plus A+B+C items"
         ((tags-todo
           "+PRIORITY=\"A\"|+PRIORITY=\"B\"|+PRIORITY=\"C\""
           ((org-agenda-sorting-strategy '(priority-down))))
          (agenda "")))
        ("A" "Agenda"
         ((agenda "")))
        ("t" "Just TODO items"
         ((tags-todo
           "+PRIORITY=\"A\"|+PRIORITY=\"B\"|+PRIORITY=\"C\""
           ((org-agenda-sorting-strategy '(priority-down))))))))
```

title: Episode 7: Free, Remix and Youtube Culture
date: 2019-01-15 14:00:00
tags: episode, free culture
summary: Free, Remix and Youtube Culture
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/007-free-remixes/librelounge-ep-007.mp3" length:"29736851" duration:"01:31:09"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/007-free-remixes/librelounge-ep-007.ogg" length:"29995274" duration:"01:31:09"
---

In this episode of Libre Lounge, Serge and Chris talk about the Free Culture movement and contrast it with other forms of Free Culture online, including the Remix movement and "Youtube culture".

Links:

- [Definition of Free Cultural Works (freedomdefined.org)](https://freedomdefined.org)
- [Karl Fogel's Talk on the History of Copyright (youtube.com)](https://www.youtube.com/watch?v=mhBpI13dxkI)
- [QuestionCopyright.org's Minute Memes (questioncopyright.org)](https://questioncopyright.org/minute_memes)
- [Youtube Copyright Issues (wikipedia)](https://en.wikipedia.org/wiki/YouTube_copyright_issues)
- [Free Culture Book by Lawrence Lessig (free-culture.cc)](http://www.free-culture.cc/)
- [Remix: Making Art and Commerce Thrive in the Hybrid Economy (archive.org)](https://archive.org/details/LawrenceLessigRemix)
- [RMS on Functional vs Non-Functional Works (gnu.org)](https://www.gnu.org/philosophy/misinterpreting-copyright.en.html)
- [RMS on the Ethics of non-Free Non-Functional Works (libervis.com)](http://www.libervis.com/article/rms_on_the_ethics_of_non_free_art)
- [Defining Non-Commercial (creativecommons.org)](https://wiki.creativecommons.org/wiki/Defining_Noncommercial)
- [The QCO Creator Endorsed Mark (questioncopyright.org)](https://questioncopyright.org/creator_endorsed)
- [The Liberated Pixel Cup (opengameart.org)](http://lpc.opengameart.org/)

title: Episode 22: Dungeons & Dragons, Free Culture, and Diversity with Sean Hillman
date: 2019-06-14 07:05:00
tags: episode, role playing games, dungeons and dragons
summary: Dungeons & Dragons, Free Culture, and Diversity with Sean Hillman
enclosure: title:ogg url:"https://media.librelounge.org/episodes/022-dnd-with-sean-hillman/librelounge-ep-022.ogg" length:"42847696" duration:"01:13:42"
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/022-dnd-with-sean-hillman/librelounge-ep-022.mp3" length:"81577378" duration:"01:13:42"
---
On this episode, Serge sits down with game creator Sean Hillman and discusses Dungeons and Dragons, the Open Gaming License and how Free Culture encourages diversity.

Links:
- [Sean Hillan's Patreon](https://www.patreon.com/Zer0MYD)
- [Zero Means You're Dead (Sean's podcast)](https://zer0meansyourdead.podbean.com/)
- [ARMZine](https://www.kickstarter.com/projects/reigndragon/armzine)
- [DIRGEZine](https://www.kickstarter.com/projects/reigndragon/dirgezine)
- [Living Greyhawk (wizards.com)](http://www.wizards.com/default.asp?x=lg/welcome)
- [The Grand Duchy of Geoff (wizards.com)](https://www.wizards.com/default.asp?x=lg/region/geoff)
- [The Open Gaming License FAQ (wizards.com)](https://www.wizards.com/default.asp?x=d20/oglfaq/20040123f)
- [FATE Game System (fate-srd.com)](https://fate-srd.com/)
- [Queens of Adventure (Matt Baume's D&D/Drag Queen Podcast)](https://queensofadventure.com/)
- [Sean O'Brien Epic Theme #1](https://soundcloud.com/stevenobrien/epic-orchestral-piece)

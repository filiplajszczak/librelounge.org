title: Episode 10: FOSDEM, CopyleftConf and Spritely
date: 2019-02-12 10:51
tags: episode, fosdem, copyleftconf, lisp, free software
summary: FOSDEM, CopyleftConf and Spritely
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/010-fosdem/librelounge-ep-010.mp3" length:"22195813" duration:"00:40:00"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/010-fosdem/librelounge-ep-010.ogg" length:"15552160" duration:"00:40:00"
---

Chris and Serge are back from FOSDEM and CopyleftConf. Chris has a grant to work on an exciting new ActivityPub application and the dynamic duo talk about recursive compilation and Lisp without parentheis.

Links:

- [FOSDEM 2019 (fosdem.org)](https://fosdem.org/2019/)
- [CopyleftConf 2019 (copyleftconf.org)](https://2019.copyleftconf.org/)
- [Chris is awarded the Samsung Stack Zero Grant (dustycloud.org)](http://dustycloud.org/blog/samsung-stack-zero-grant/)
- [Spritely (gitlab.org)](https://gitlab.com/spritely)
- [A Guiler's Year of Racket (fosdem.org)](https://fosdem.org/2019/schedule/event/guileracket/)
- [ActivityPub panel (fosdem.org)](https://fosdem.org/2019/schedule/event/activitypub_panel/)
- [GNU Mes (fosdem.org)](https://fosdem.org/2019/schedule/event/gnumes/)
- [Experience with wisp (fosdem.org)](https://fosdem.org/2019/schedule/event/experiencewithwisp/)



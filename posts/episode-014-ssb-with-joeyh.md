title: Episode 14: Secure Scuttlebutt with Joey Hess
date: 2019-03-29 00:40
tags: episode, libreplanet, ssb
summary: Secure Scuttlebutt with Joey Hess
enclosure: title:mp3 url:"https://media.librelounge.org/episodes/014-ssb-with-joeyh/librelounge-ep-014.mp3" length:"41431781" duration:"00:46:24"
enclosure: title:ogg url:"https://media.librelounge.org/episodes/014-ssb-with-joeyh/librelounge-ep-014.ogg" length:"27478116" duration:"00:46:24"
---
Libre Lounge comes to you with an interview from Libre Planet with Joey Hess discussing the Secure Scuttbutt project, a secure social network. The interview goes into detail about the protocol, differences between SSB and ActivityPub, and how Secure Scuttlebutt is a bit like Git.

Links:

- [Joey Hess](https://joeyh.name/)
- [Scuttlebutt](https://www.scuttlebutt.nz/)
- [Secure Scuttlebutt Protocol](https://ssbc.github.io/docs/)
- [Git Annex](https://git-annex.branchable.com/)
- [Libre Planet](https://libreplanet.org)
- [FSF (thanks for the batteries!)](https://www.fsf.org/)




